package com.example.applivoyage;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.json.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static javafx.application.Application.launch;

public class EnregistrementController extends Application {

    private static Stage stages;


    private  String path="applivoyage\\src\\main\\resources\\data\\compte.json";
    @FXML
    private TextField nomcompte;

    @FXML
    private TextField prenomcompte;

    @FXML
    private ToggleGroup role;

    @FXML
    private RadioButton roleagent;

    @FXML
    private RadioButton roleclient;

    @FXML
    void creationcompte(MouseEvent event) throws IOException {
        RadioButton button = (RadioButton) role.getSelectedToggle();
        if (nomcompte.getText()==null|| prenomcompte.getText()==null||button==null){
            // créer une pop up qui averti l'utilisateur de remplir tout les champs
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("ERREUR");
            alert.setContentText(" vous devez Remplir tout les champs!");
            alert.showAndWait();
        }else{
            //recupérer tout les données du fichier
            String text = new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8);
            //convertir le fichier en json
            JSONArray lescomptes=new JSONArray(text);

            //créer un objet json
            JSONObject obj=new JSONObject();
            obj.put("nom",nomcompte.getText());
            obj.put("prenom",prenomcompte.getText());
            obj.put("role",button.getText());
            //ajouter le compte dans le tableaux des comptes
            lescomptes.put(obj);

            try (FileWriter file = new FileWriter(path)) {
                //écrire dans le fichier
                file.write(lescomptes.toString());
                file.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }



            // pop up pour annoncer  a l utilisateur que son compte a été créer
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Création compte");
            alert.setContentText(" la creation a été effectuer vous pouvez  connectée!");
            alert.showAndWait();

            //changement de fenetre
            Parent pane = FXMLLoader.load(getClass().getResource("connexion.fxml"));
            Scene scene = new Scene( pane );
            stages=(Stage)((Node)event.getSource()).getScene().getWindow();
            stages.setScene((scene));
            stages.show();

        }


    }
    @FXML
    void retourecran(MouseEvent event) throws IOException {
        //changement de fenetre
        Parent pane = FXMLLoader.load(getClass().getResource("connexion.fxml"));
        Scene scene = new Scene( pane );
        stages=(Stage)((Node)event.getSource()).getScene().getWindow();
        stages.setScene((scene));
        stages.show();
    }

    @Override
    public void start(Stage stage) throws Exception {
        stages=stage;
        stages.setTitle("Enregistrement");
    }

}
