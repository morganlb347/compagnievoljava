module com.example.applivoyage {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.json;


    opens com.example.applivoyage to javafx.fxml;
    exports com.example.applivoyage;
}